/* ********************************************************************
    itom software
    URL: http://www.uni-stuttgart.de/ito
    Copyright (C) 2016, Institut fuer Technische Optik (ITO),
    Universitaet Stuttgart, Germany

    This file is part of itom.
  
    itom is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public Licence as published by
    the Free Software Foundation; either version 2 of the Licence, or (at
    your option) any later version.

    itom is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
    General Public Licence for more details.

    You should have received a copy of the GNU Library General Public License
    along with itom. If not, see <http://www.gnu.org/licenses/>.
*********************************************************************** */

#ifndef PYTHONPLUGINS
#define PYTHONPLUGINS

#include <string>

#ifndef Q_MOC_RUN
    /* includes */
    //python
    // see http://vtk.org/gitweb?p=VTK.git;a=commitdiff;h=7f3f750596a105d48ea84ebfe1b1c4ca03e0bab3
    #if (defined _DEBUG) && (defined WIN32)
        #undef _DEBUG
        #include "Python.h"
        #define _DEBUG
    #else
        #include "Python.h"
    #endif
#endif

#include "../common/addInInterface.h"

namespace ito
{

/** @class PythonPlugins
*   @brief  class summing up the functionality of itom - hardware python plugins
*
*   There exist three different types of plugins for the itom program:
*       - actuator plugins  (everything that moves)
*       - dataIO plugins (in- and output of data, e.g. ADDA cards, frame grabber, cameras, spectrometers, ...)
*       - algo plugins (pure software evaluations / calculations)
*
*   The python interfaces for these plugins are declared in this class. For a more detailed description about
*   the plugin interface their handling and so on see the documentation of the according classes \ref AddInInterfaceBase,
*   \ref AddInBase, \ref AddInActuator, \ref AddInDataIO, \ref AddInAlgo and \ref AddInManager.
*/
class PythonPlugins
{
   public:
       typedef struct
       {
           PyObject_HEAD
           ito::AddInActuator *actuatorObj;
           PyObject* base;
           PyObject *weakreflist; /* List of weak references */
       }
       PyActuatorPlugin;

       typedef struct
       {
           PyObject_HEAD
           ito::AddInDataIO *dataIOObj;
           PyObject* base;
           PyObject *weakreflist; /* List of weak references */
       }
       PyDataIOPlugin;
       
       // Actuator
       static void PyActuatorPlugin_dealloc(PyActuatorPlugin *self);
       static PyObject *PyActuatorPlugin_new(PyTypeObject *type, PyObject *args, PyObject *kwds);
       static int PyActuatorPlugin_init(PyActuatorPlugin *self, PyObject *args, PyObject *kwds);
       static PyObject* PyActuatorPlugin_repr(PyActuatorPlugin *self);

       static PyObject *PyActuatorPlugin_name(PyActuatorPlugin *self);
       static PyObject *PyActuatorPlugin_getParamList(PyActuatorPlugin *self);
       static PyObject *PyActuatorPlugin_getParamListInfo(PyActuatorPlugin *self,  PyObject *args);
       static PyObject* PyActuatorPlugin_getExecFuncsInfo(PyActuatorPlugin* self, PyObject *args, PyObject *kwds);
       static PyObject *PyActuatorPlugin_getParam(PyActuatorPlugin *self, PyObject *args);
       static PyObject *PyActuatorPlugin_getParamInfo(PyActuatorPlugin *self, PyObject *args);
       static PyObject *PyActuatorPlugin_setParam(PyActuatorPlugin *self, PyObject *args);
       static PyObject *PyActuatorPlugin_getType(PyActuatorPlugin *self);
       static PyObject *PyActuatorPlugin_execFunc(PyActuatorPlugin *self, PyObject *args, PyObject *kwds);
       static PyObject *PyActuatorPlugin_showConfiguration(PyActuatorPlugin *self);
       static PyObject *PyActuatorPlugin_showToolbox(PyActuatorPlugin *self);
       static PyObject *PyActuatorPlugin_hideToolbox(PyActuatorPlugin *self);

       static PyObject *PyActuatorPlugin_setInterrupt(PyActuatorPlugin *self);

       static PyObject *PyActuatorPlugin_calib(PyActuatorPlugin *self, PyObject *args);
       static PyObject *PyActuatorPlugin_setOrigin(PyActuatorPlugin *self, PyObject *args);
       static PyObject *PyActuatorPlugin_getStatus(PyActuatorPlugin *self, PyObject *args);
       static PyObject *PyActuatorPlugin_getPos(PyActuatorPlugin *self, PyObject *args);
       static PyObject *PyActuatorPlugin_setPosAbs(PyActuatorPlugin *self, PyObject *args);
       static PyObject *PyActuatorPlugin_setPosRel(PyActuatorPlugin *self, PyObject *args);

       static PyMemberDef  PyActuatorPlugin_members[];
       static PyMethodDef  PyActuatorPlugin_methods[];
       static PyTypeObject PyActuatorPluginType;
       static PyModuleDef  PyActuatorPluginModule;

       static void paramBaseVectorDeleter(QVector<ito::ParamBase> *obj)
       {
           delete obj;
       }

       //DataIO
       static void PyDataIOPlugin_dealloc(PyDataIOPlugin *self);
       static PyObject *PyDataIOPlugin_new(PyTypeObject *type, PyObject *args, PyObject *kwds);
       static int PyDataIOPlugin_init(PyDataIOPlugin *self, PyObject *args, PyObject *kwds);
       static PyObject* PyDataIOPlugin_repr(PyDataIOPlugin *self);

       static PyObject *PyDataIOPlugin_name(PyDataIOPlugin *self);
       static PyObject *PyDataIOPlugin_getParamList(PyDataIOPlugin *self);
       static PyObject* PyDataIOPlugin_getParamListInfo(PyDataIOPlugin* self,  PyObject *args);
       static PyObject* PyDataIOPlugin_getExecFuncsInfo(PyDataIOPlugin* self, PyObject *args, PyObject *kwds);
       static PyObject *PyDataIOPlugin_getParam(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_getParamInfo(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_setParam(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_getType(PyDataIOPlugin *self);
       static PyObject *PyDataIOPlugin_execFunc(PyDataIOPlugin *self, PyObject *args, PyObject *kwds);
       static PyObject *PyDataIOPlugin_showConfiguration(PyDataIOPlugin *self);
       static PyObject *PyDataIOPlugin_showToolbox(PyDataIOPlugin *self);
       static PyObject *PyDataIOPlugin_hideToolbox(PyDataIOPlugin *self);

       static PyObject *PyDataIOPlugin_startDevice(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_stopDevice(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_acquire(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_getVal(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_copyVal(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_setVal(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_enableAutoGrabbing(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_disableAutoGrabbing(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_setAutoGrabbing(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_getAutoGrabbing(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_setAutoGrabbingInterval(PyDataIOPlugin *self, PyObject *args);
       static PyObject *PyDataIOPlugin_getAutoGrabbingInterval(PyDataIOPlugin *self);
       
       static PyMemberDef  PyDataIOPlugin_members[];
       static PyMethodDef  PyDataIOPlugin_methods[];
       static PyTypeObject PyDataIOPluginType;
       static PyModuleDef  PyDataIOPluginModule;
       static void PyDataIOPlugin_addTpDict(PyObject *tp_dict);

};

} //end namespace ito

#endif
