<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>QObject</name>
    <message>
        <source>Live data source for plot</source>
        <translation type="obsolete">Live Datenquelle für Plot</translation>
    </message>
    <message>
        <source>Source data for plot</source>
        <translation type="obsolete">Quelldaten für Plot</translation>
    </message>
    <message>
        <source>Actual output data of plot</source>
        <translation type="obsolete">Aktuelle Ausgabedaten für Plot</translation>
    </message>
    <message>
        <source>Parameter: does not exist in updateParam</source>
        <translation type="obsolete">Parameter: Existiert nicht in &apos;updateParam&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="71"/>
        <source>Tried to scale unscaleable unit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="75"/>
        <source>No unit specified</source>
        <translation type="unfinished">Keine Einheiten festgelegt</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="206"/>
        <source>Pluginname undefined. No xml file loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="243"/>
        <source>ParamList not inialized properly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="488"/>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="704"/>
        <source>Can&apos;t open xml file</source>
        <translation type="unfinished">XML-Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="776"/>
        <source>%1
Autosave parameter %2 not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="780"/>
        <source>XML-Import warnings:
Autosave parameter %1 not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="807"/>
        <source>%1
Obsolete parameter %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="811"/>
        <source>XML-Import warnings:
Obsolete parameter %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="820"/>
        <source>%1
Parameter %2 not autosave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="824"/>
        <source>XML-Import warnings:
Parameter %1 not autosave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="849"/>
        <source>%1
Parameter not loadable %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="853"/>
        <source>XML-Import warnings:
Parameter not loadable %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="863"/>
        <source>%1
Type conflict for %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="867"/>
        <source>XML-Import warnings:
Type conflict for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="944"/>
        <source>Save object failed: type not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1238"/>
        <source>Save object failed: invalid object handle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1243"/>
        <source>Save object failed: object seems empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1278"/>
        <source>Save object failed: file not writeable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1373"/>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1402"/>
        <source>Load object warning: Metadata &quot; %1 &quot; for %2 missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1438"/>
        <source>Load object failed: Number of dims smaller 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1446"/>
        <source>Not enough memory to alloc sizes vector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1462"/>
        <source>Load object failed: dimension size missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1476"/>
        <source>Load object failed: dimX not specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1488"/>
        <source>Load object failed: dimY not specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1563"/>
        <source>Load object failed: type not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1572"/>
        <source>Load object failed: Error during allocating memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1617"/>
        <source>Load object failed: file corrupted at metaData (v1.0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1624"/>
        <source>Load object warning: file has invalid metaData for v1.0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1642"/>
        <source>Load object warning: DoubleExportType for v1.0 invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1647"/>
        <source>Load object warning: DoubleExportType for v1.0 missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1681"/>
        <source>Load object warning: MetaData for %1 missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1714"/>
        <source>Load object warning: MetaData for dimX missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1744"/>
        <source>Load object warning: MetaData for dimY missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1774"/>
        <source>Load object warning: MetaData for values missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1803"/>
        <source>Load object warning: MetaData import for Rotation Matrix failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1838"/>
        <source>Load object failed: file corrupted at tagSpace (v1.0)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1843"/>
        <source>Load object failed: tag space not at expected position. Got %1 instead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1856"/>
        <source>Load object failed: tags Space invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1894"/>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1900"/>
        <source>Load object warning: invalid tagType found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1906"/>
        <source>Load object warning: tagsSpace invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1945"/>
        <source>Load object failed: dataSpace missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1950"/>
        <source>Load object failed: dataSpace not at expected position. Got %1 instead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1960"/>
        <source>Load object warning: dataSpace and dataObject are not equal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1965"/>
        <source>Load object warning: dataSpace attributes corrupted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1978"/>
        <source>Load object warning: dataSpace for a plane corrupted. Got %1 instead of %2 bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="1989"/>
        <source>Load object failed: dataStream ended before finished reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2019"/>
        <source>Load object failed: invalid object handle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2046"/>
        <source>Load object failed: file not readable or does not exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2060"/>
        <source>Load object failed: file seems corrupt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2068"/>
        <source>Load object failed: wrong xml version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2076"/>
        <source>Load object failed: wrong document encoding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2082"/>
        <source>Load object failed: unexpected file ending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2101"/>
        <source>Load object failed: file is no itomDataObjectFile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2129"/>
        <source>Load object failed: illegal format version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/sharedFunctionsQt.cpp" line="2134"/>
        <source>Load object failed: object header not valied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="63"/>
        <source>no or invalid plugin given.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="77"/>
        <source>no or invalid plugin given</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="154"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="170"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="175"/>
        <source>Timeout while waiting for answer from camera.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="204"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="241"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="299"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="324"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="351"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="395"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="420"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="447"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="475"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="503"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="530"/>
        <source>no camera available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="211"/>
        <source>error invoking getParam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="222"/>
        <source>no parameter can be returned if timeout = 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="248"/>
        <source>error invoking setParam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="305"/>
        <source>error invoking startDevice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="330"/>
        <source>error invoking stopDevice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="357"/>
        <source>error invoking acquire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="401"/>
        <source>error invoking enableAutoGrabbing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="426"/>
        <source>error invoking disableAutoGrabbing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="453"/>
        <source>error invoking setAutoGrabbingInterval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="481"/>
        <source>error invoking getVal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="509"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="541"/>
        <source>error invoking copyVal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="554"/>
        <source>no image parameters can be returned if timeout = 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="612"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="648"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="683"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="713"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="739"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="778"/>
        <source>no actuator available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="617"/>
        <source>Error during setPosRel: Vectors differ in size</source>
        <translation type="unfinished">Fehler bei setPosRel: Vektoren unterscheiden sich in der Größe</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="623"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="689"/>
        <source>error invoking setPosRel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="653"/>
        <source>Error during setPosAbs: Vectors differ in size</source>
        <translation type="unfinished">Fehler bei setPosAbs: Vektoren unterscheiden sich in der Größe</translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="659"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="719"/>
        <source>error invoking setPosAbs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="748"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="788"/>
        <source>error invoking getPos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="758"/>
        <source>no position value can be returned if timeout = 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="799"/>
        <source>no position value(s) can be returned if timeout = 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="825"/>
        <location filename="../../common/sources/pluginThreadCtrl.cpp" line="831"/>
        <source>failed to ask for number of axes of actuator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="47"/>
        <source>parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="68"/>
        <source>mandatory parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="72"/>
        <source>optional parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="76"/>
        <source>output parameter vector is not initialized</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="106"/>
        <location filename="../../common/sources/helperCommon.cpp" line="127"/>
        <source>parameter &apos;%1&apos; cannot be found in given parameter vector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="207"/>
        <location filename="../../common/sources/helperCommon.cpp" line="332"/>
        <source>name of requested parameter is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="218"/>
        <location filename="../../common/sources/helperCommon.cpp" line="343"/>
        <source>the parameter name &apos;%1&apos; is invald</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="242"/>
        <source>array index of parameter out of bounds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="249"/>
        <location filename="../../common/sources/helperCommon.cpp" line="376"/>
        <source>given index of parameter name ignored since parameter is no array type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="256"/>
        <location filename="../../common/sources/helperCommon.cpp" line="383"/>
        <source>parameter not found in m_params.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="369"/>
        <source>array index out of bounds.</source>
        <translation type="unfinished">Array-Index liegt außerhalb des Bereichs.</translation>
    </message>
    <message>
        <location filename="../../common/sources/helperCommon.cpp" line="424"/>
        <source>invalid parameter name</source>
        <translation type="unfinished">Ungültiger Parametername</translation>
    </message>
</context>
<context>
    <name>ito::AbstractAddInConfigDialog</name>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="81"/>
        <source>slot &apos;setParam&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParam&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="86"/>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="154"/>
        <source>pointer to plugin is invalid.</source>
        <translation type="unfinished">Zeiger des Plugins ist ungültig.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="100"/>
        <source>Error while setting parameter &apos;%1&apos;</source>
        <translation type="unfinished">Fehler beim Parametersetzen &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="104"/>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="165"/>
        <source>Error while setting parameter</source>
        <translation type="unfinished">Fehler beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="118"/>
        <source>Warning while setting parameter &apos;%1&apos;</source>
        <translation type="unfinished">Warnung beim Parametersetzen &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="122"/>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="176"/>
        <source>Warning while setting parameter</source>
        <translation type="unfinished">Warnung beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="149"/>
        <source>slot &apos;setParamVector&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParamVector&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="201"/>
        <source>Timeout while waiting for answer from plugin instance.</source>
        <translation type="unfinished">Zeitüberschreitung beim Warten auf Antwort der Plugin-Instanz.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="214"/>
        <source>Error while execution</source>
        <translation type="unfinished">Fehler bei der Ausführung</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInConfigDialog.cpp" line="225"/>
        <source>Warning while execution</source>
        <translation type="unfinished">Warnung bei der Ausführung</translation>
    </message>
</context>
<context>
    <name>ito::AbstractAddInDockWidget</name>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="81"/>
        <source>slot &apos;setParam&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParam&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="86"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="134"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="242"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="300"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="350"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="397"/>
        <source>pointer to plugin is invalid.</source>
        <translation type="unfinished">Zeiger des Plugins ist ungültig.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="92"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="140"/>
        <source>Error while setting parameter</source>
        <translation type="unfinished">Fehler beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="103"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="151"/>
        <source>Warning while setting parameter</source>
        <translation type="unfinished">Warnung beim Parametersetzen</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="129"/>
        <source>slot &apos;setParamVector&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setParamVector&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="176"/>
        <source>Timeout while waiting for answer from plugin instance.</source>
        <translation type="unfinished">Zeitüberschreitung beim Warten auf Antwort der Plugin-Instanz.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="189"/>
        <source>Error while execution</source>
        <translation type="unfinished">Fehler bei der Ausführung</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="200"/>
        <source>Warning while execution</source>
        <translation type="unfinished">Warnung bei der Ausführung</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="224"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="282"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="338"/>
        <source>setActuatorPosition can only be called for actuator plugins</source>
        <translation type="unfinished">Der Parameter &apos;setActuatorPosition&apos; kann nur für Motor-Plugins aufgerufen werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="236"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="294"/>
        <source>slot &apos;%1&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;%1&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="248"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="306"/>
        <source>Error while calling %1</source>
        <translation type="unfinished">Fehler beim Aufruf von %1</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="259"/>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="317"/>
        <source>Warning while calling %1</source>
        <translation type="unfinished">Warnung beim Aufruf von %1</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="344"/>
        <source>slot &apos;requestStatusAndPosition&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;requestStatusAndPosition&apos; kann nicht aufgerufen werden, da dieser nicht exisitert.</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="356"/>
        <source>Error while calling &apos;requestStatusAndPosition&apos;</source>
        <translation type="unfinished">Fehler beim Aufruf von  &apos;requestStatusAndPosition&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="367"/>
        <source>Warning while calling &apos;requestStatusAndPosition&apos;</source>
        <translation type="unfinished">Warnung beim Aufruf von  &apos;requestStatusAndPosition&apos;</translation>
    </message>
    <message>
        <location filename="../../common/sources/abstractAddInDockWidget.cpp" line="388"/>
        <source>setActuatorInterrupt can only be called for actuator plugins</source>
        <translation type="unfinished">Der Parameter &apos;setActuatorInterrupt&apos; kann nur für Motor-Plugins aufgerufen werden</translation>
    </message>
</context>
<context>
    <name>ito::AddInAlgo</name>
    <message>
        <location filename="../../common/addInInterface.h" line="1001"/>
        <source>uninitialized vector for mandatory parameters!</source>
        <translation type="unfinished">Nicht initialisierter Vektor für Pflichtparameter!</translation>
    </message>
    <message>
        <location filename="../../common/addInInterface.h" line="1005"/>
        <source>uninitialized vector for optional parameters!</source>
        <translation type="unfinished">Nicht initialisierter Vektor für optionale Parameter!</translation>
    </message>
    <message>
        <location filename="../../common/addInInterface.h" line="1009"/>
        <source>uninitialized vector for output parameters!</source>
        <translation type="unfinished">Nicht initialisierter Vektor für Ausgabeparameter!</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="1077"/>
        <source>Constructor must be overwritten</source>
        <translation type="unfinished">Der Konstruktor muss überschrieben sein</translation>
    </message>
</context>
<context>
    <name>ito::AddInBase</name>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="390"/>
        <source>function execution unused in this plugin</source>
        <translation type="unfinished">Die Funktion &apos;execution&apos; wird in diesem Plugin nicht benutzt</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="435"/>
        <source>Toolbox</source>
        <translation type="unfinished">Werkzeuge</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="587"/>
        <source>function with this name is already registered.</source>
        <translation type="unfinished">Es existiert bereits eine Funktion mit diesem Namen.</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="646"/>
        <source>Your plugin is supposed to have a configuration dialog, but you did not implement the showConfDialog-method</source>
        <translation type="unfinished">Das Plugin hat vermutlich einen Konfigurationsdialog, aber die Methode &apos;showConfDialog&apos; wurde nicht implementiert</translation>
    </message>
</context>
<context>
    <name>ito::AddInDataIO</name>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="709"/>
        <source>listener does not have a slot </source>
        <translation type="unfinished">&apos;Listener&apos; hat keinen Slot </translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="713"/>
        <source>this object already has been registered as listener</source>
        <translation type="unfinished">Diese Objekt wurde bereits beim &apos;Listener&apos; registriert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="725"/>
        <source>timer could not be set</source>
        <translation type="unfinished">Timer kann nicht gesetzt werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="750"/>
        <source>the object could not been removed from the listener list</source>
        <translation type="unfinished">Das Objekt kann nicht aus der &apos;Listener&apos;-Liste entfernt werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="843"/>
        <source>empty interval buffer has been given</source>
        <translation type="unfinished">Es wurde ein leerer Bereichspuffer übergeben</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="882"/>
        <location filename="../../common/sources/addInInterface.cpp" line="902"/>
        <location filename="../../common/sources/addInInterface.cpp" line="920"/>
        <location filename="../../common/sources/addInInterface.cpp" line="938"/>
        <location filename="../../common/sources/addInInterface.cpp" line="956"/>
        <location filename="../../common/sources/addInInterface.cpp" line="974"/>
        <location filename="../../common/sources/addInInterface.cpp" line="992"/>
        <source>not implemented</source>
        <translation type="unfinished">Nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="888"/>
        <source>method startDevice() is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;startDevice&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="906"/>
        <source>method stopDevice() is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;stopDevice&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="924"/>
        <source>method acquire() is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;acquire&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="942"/>
        <source>method getVal(void*, ItomSharedSemaphore*) is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;getVal(void*, ItomSharedSemaphore*)&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="960"/>
        <source>method getVal(QSharedPointer&lt;char&gt;, QSharedPointer&lt;int&gt;, ItomSharedSemaphore*) is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;getVal(QSharedPointer&lt;char&gt;, QSharedPointer&lt;int&gt;, ItomSharedSemaphore*)&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="978"/>
        <source>method copyVal(void*,ItomSharedSemaphore*) is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;copyVal(void*,ItomSharedSemaphore*)&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInInterface.cpp" line="996"/>
        <source>method setVal(const char*, const int, ItomSharedSemaphore*) is not implemented in this plugin</source>
        <translation type="unfinished">Die Methode &apos;setVal(const char*, const int, ItomSharedSemaphore*)&apos; wurde in diesem Plugin nicht implementiert</translation>
    </message>
</context>
<context>
    <name>ito::AddInGrabber</name>
    <message>
        <location filename="../../common/sources/addInGrabber.cpp" line="89"/>
        <location filename="../../common/sources/addInGrabber.cpp" line="104"/>
        <source>slot &apos;setSource&apos; of live source node could not be invoked</source>
        <translation type="unfinished">Der Slot &apos;setSource&apos; kann nicht aufgerufen werden</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInGrabber.cpp" line="234"/>
        <source>Error during check data, external dataObject invalid. Object has more or less than 1 plane. It must be of right size and type or an uninitilized image.</source>
        <translation type="unfinished">Fehler beim überprüfen der Daten. Das externe Datenobjekt ist ungültig. Das Objekt hat mehr oder weniger als eine Ebene. Es muss die richtige Größe und vom richtigen Typ sein oder ein nicht initialisiertes Image.</translation>
    </message>
    <message>
        <location filename="../../common/sources/addInGrabber.cpp" line="238"/>
        <source>Error during check data, external dataObject invalid. Object must be of right size and type or an uninitilized image.</source>
        <translation type="unfinished">Fehler beim überprüfen der Daten. Das externe Datenobjekt ist ungültig. Das Objekt muss die richtige Größe und vom richtigen Typ sein oder ein nicht initialisiertes Image.</translation>
    </message>
    <message>
        <source>Error during check data, external dataObject invalid. Object must be of right size and type or a uninitilized image.</source>
        <translation type="obsolete">Fehler beim überprüfen der Daten. Das externe Datenobjekt ist ungültig. Das Objekt muss die richtige Größe und vom richtigen Typ sein oder ein nicht initialisiertes Image.</translation>
    </message>
</context>
</TS>
