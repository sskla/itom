<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>DoubleRangeWidget</name>
    <message>
        <source>SliderSpinBoxWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MenuComboBox</name>
    <message>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
</context>
<context>
    <name>MenuComboBoxPrivate</name>
    <message>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
</context>
<context>
    <name>MotorAxisController</name>
    <message>
        <source>MotorAxisController</source>
        <translation></translation>
    </message>
    <message>
        <source>Absolute movement</source>
        <translation>Absolute Fahrt</translation>
    </message>
    <message>
        <source>Relative movement</source>
        <translation>Relative Fahrt</translation>
    </message>
    <message>
        <source>All movements</source>
        <translation>Absolut und Relativ</translation>
    </message>
    <message>
        <source>Only monitor</source>
        <translation>Nur überwachen</translation>
    </message>
    <message>
        <source>Start all axes</source>
        <translation>Alle Achsen starten</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbruch</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>Aktualisieren</translation>
    </message>
    <message>
        <source>Current Pos.</source>
        <translation>Aktuelle Pos.</translation>
    </message>
    <message>
        <source>Target Pos.</source>
        <translation>Zielposition</translation>
    </message>
    <message>
        <source>Abs.</source>
        <translation></translation>
    </message>
    <message>
        <source>Step Size</source>
        <translation>Schrittweite</translation>
    </message>
    <message>
        <source>Rel.</source>
        <translation></translation>
    </message>
    <message>
        <source>up</source>
        <translation type="unfinished">Auf</translation>
    </message>
    <message>
        <source>down</source>
        <translation type="unfinished">Ab</translation>
    </message>
    <message>
        <source>go</source>
        <translation type="unfinished">Start</translation>
    </message>
    <message>
        <source>slot &apos;requestStatusAndPosition&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;requestStatusAndPosition&apos; kann nicht aufgerufen werden weil dieser nicht existiert.</translation>
    </message>
    <message>
        <source>pointer to plugin is invalid.</source>
        <translation type="unfinished">Der Pointer des Plugins ist ungültig.</translation>
    </message>
    <message>
        <source>slot &apos;setPosAbs&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;setPosAbs&apos; kann nicht aufgerufen werden weil dieser nicht existiert.</translation>
    </message>
    <message>
        <source>slot &apos;%s&apos; could not be invoked since it does not exist.</source>
        <translation type="unfinished">Der Slot &apos;%s&apos; kann nicht aufgerufen werden weil dieser nicht existiert.</translation>
    </message>
    <message>
        <source>Timeout while waiting for answer from plugin instance.</source>
        <translation type="unfinished">Zeitüberschreitung. Die Plugin-Instanz antwortet nicht.</translation>
    </message>
    <message>
        <source>Error while calling &apos;%1&apos;</source>
        <translation type="unfinished">Fehler beim Aufruf von &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Warning while calling &apos;%1&apos;</source>
        <translation type="unfinished">Warnung beim Aufruf von &apos;%1&apos;</translation>
    </message>
    <message>
        <source>Unit</source>
        <translation type="unfinished">Einheit</translation>
    </message>
    <message>
        <source>Decimals</source>
        <translation type="unfinished">Dezimal</translation>
    </message>
    <message>
        <source>axisIndex is out of bounds.</source>
        <translation type="unfinished">Der Achsenindex liegt außerhalb des Gültigkeitsbereichs.</translation>
    </message>
    <message>
        <source>refresh</source>
        <translation type="unfinished">Aktualisieren</translation>
    </message>
    <message>
        <source>Actuator not available</source>
        <translation type="unfinished">Der Motor ist nicht verfügbar</translation>
    </message>
    <message>
        <source>interrupt movement</source>
        <translation type="unfinished">Fahrt unterbrochen</translation>
    </message>
    <message>
        <source>start movement</source>
        <translation type="unfinished">Fahrt starten</translation>
    </message>
</context>
<context>
    <name>PathLineEdit</name>
    <message>
        <source>Select a file to save </source>
        <translation>Dateiname angeben</translation>
    </message>
    <message>
        <source>Open a file</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <source>Select a directory...</source>
        <translation>Ein Verzeichnis auswählen...</translation>
    </message>
</context>
<context>
    <name>PathLineEditPrivate</name>
    <message>
        <source>Open a dialog</source>
        <translation>Dialog öffnen</translation>
    </message>
</context>
<context>
    <name>PlotInfoMarker</name>
    <message>
        <source>Property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <source>Marker %1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PlotInfoPicker</name>
    <message>
        <source>Property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>PlotInfoShapes</name>
    <message>
        <source>Property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Basic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RangeWidget</name>
    <message>
        <source>SliderSpinBoxWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>SearchBoxPrivate</name>
    <message>
        <source>Search...</source>
        <translation>Suchen...</translation>
    </message>
</context>
<context>
    <name>SliderWidget</name>
    <message>
        <source>SliderWidget</source>
        <translation></translation>
    </message>
</context>
</TS>
