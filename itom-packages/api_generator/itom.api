itom.BUTTON
itom.MENU
itom.SEPARATOR
itom.actuator(name[, mandparams, optparams]) -> creates new instance of actuator plugin 'name'
itom.actuator.calib(axis[, axis1, ...]) -> starts calibration or homing of given axes (0-based).
itom.actuator.exec(funcName [, param1, ...]) -> invoke the function 'funcName' registered as execFunc within the plugin.
itom.actuator.getExecFuncsInfo([funcName [, detailLevel]]) -> plots a list of available execFuncs or a detailed description of the specified execFunc.
itom.actuator.getParam(name) -> current value of the plugin parameter 'name'.
itom.actuator.getParamInfo(name) -> returns dictionary with meta information of parameter 'name'.
itom.actuator.getParamList() -> returns a list of the names of the internal parameters of the plugin
itom.actuator.getParamListInfo([detailLevel]) -> prints detailed information about all plugin parameters.
itom.actuator.getPos(axis[, axis1, ...]) -> returns the actual positions of the given axes (in mm or degree).
itom.actuator.getStatus() -> returns a list of status values for each axis
itom.actuator.getType() -> returns actuator type
itom.actuator.hideToolbox() -> hides toolbox of the plugin
itom.actuator.name() -> returns the plugin name
itom.actuator.setInterrupt() -> interrupts a movement of an actuator
itom.actuator.setOrigin(axis[, axis1, ...]) -> defines the actual position of the given axes to value 0.
itom.actuator.setParam(name, value) -> sets parameter 'name' to the given value.
itom.actuator.setPosAbs(axis0, pos0 [, axis1, pos1, ...]) -> moves given axes to given absolute values (in mm or degree).
itom.actuator.setPosRel(axis0, pos0[, axis1, pos1, ...]) -> relatively moves given axes by the given distances [in mm or degree].
itom.actuator.showConfiguration() -> show configuration dialog of the plugin
itom.actuator.showToolbox() -> open toolbox of the plugin
itom.addButton(toolbarName, buttonName, code [, icon, argtuple]) -> adds a button to a toolbar in the main window
itom.addMenu(type, key [, name, code, icon, argtuple]) -> adds an element to the menu bar of itom.
itom.autoInterval([min=-inf, max=inf, auto=true]) -> creates a new auto interval object.
itom.autoInterval.auto
itom.autoInterval.max
itom.autoInterval.min
itom.autoInterval.name
itom.autoReloader(enabled [,checkFileExec = True, checkCmdExec = True, checkFctExec = False]) -> dis-/enables the module to automatically reload changed modules
itom.checkSignals
itom.close(handle|'all') -> method to close any specific or all open figures (unless any figure-instance still keeps track of them)
itom.compressData(str) -> compresses the given string using the method qCompress
itom.dataIO(name[, mandparams, optparams]) -> creates new instance of dataIO plugin 'name'
itom.dataIO.TRIGGER_SOFTWARE
itom.dataIO.acquire(trigger=dataIO.TRIGGER_SOFTWARE) -> triggers a new the camera acquisition
itom.dataIO.copyVal(dataObject) -> gets deep copy of data of this plugin, stored in the given data object.
itom.dataIO.disableAutoGrabbing() -> Disable auto grabbing for the grabber (camera...),
itom.dataIO.enableAutoGrabbing() -> enable auto grabbing for the grabber (camera...),
itom.dataIO.exec(funcName [, param1, ...]) -> invoke the function 'funcName' registered as execFunc within the plugin.
itom.dataIO.getAutoGrabbing() -> return the status of the auto grabbing flag.
itom.dataIO.getAutoGrabbingInterval() -> Returns the current auto grabbing interval (in ms), even if auto grabbing is disabled
itom.dataIO.getExecFuncsInfo([funcName [, detailLevel]]) -> plots a list of available execFuncs or a detailed description of the specified execFunc.
itom.dataIO.getParam(name) -> current value of the plugin parameter 'name'.
itom.dataIO.getParamInfo(name) -> returns dictionary with meta information of parameter 'name'.
itom.dataIO.getParamList() -> returns a list of the names of the internal parameters of the plugin
itom.dataIO.getParamListInfo([detailLevel]) -> prints detailed information about all plugin parameters.
itom.dataIO.getType() -> returns dataIO type
itom.dataIO.getVal(buffer=`dataObject`|`bytearray`|`bytes` [,length=maxlength]) -> returns shallow copy of internal camera image if `dataObject`-buffer is provided. Else values from plugin are copied to given byte or byte-array buffer.
itom.dataIO.hideToolbox() -> hides toolbox of the plugin
itom.dataIO.name() -> returns the plugin name
itom.dataIO.setAutoGrabbing(on) -> Set auto grabbing of the grabber device to on or off
itom.dataIO.setAutoGrabbingInterval() -> Sets the current auto grabbing interval (in ms) without dis- or enabling the auto grabber
itom.dataIO.setParam(name, value) -> sets parameter 'name' to the given value.
itom.dataIO.setVal(dataObjectOrBuffer [, length=1]) -> transfer given `dataObject` to ADDA plugin or further buffer to other dataIO plugin.
itom.dataIO.showConfiguration() -> show configuration dialog of the plugin
itom.dataIO.showToolbox() -> open toolbox of the plugin
itom.dataIO.startDevice([count=1]) -> starts the given dataIO-plugin.
itom.dataIO.stopDevice([count=1]) -> stops the given dataIO-plugin.
itom.dataObject([dims [, dtype='uint8'[, continuous = 0][, data = valueOrSequence]]]) -> constructor to get a new dataObject.
itom.dataObject.abs() -> return a new data object with the absolute values of the source
itom.dataObject.addToProtocol(newLine) -> Appends a protocol line to the protocol.
itom.dataObject.adj() -> Adjugate all elements
itom.dataObject.adjugate() -> returns the plane-wise adjugated array of this dataObject.
itom.dataObject.adjustROI(offsetList) -> adjust the size and position of the region of interest of this data object
itom.dataObject.arg() -> return a new data object with the argument values of the source
itom.dataObject.astype(typestring) -> converts this data object to another type
itom.dataObject.axisDescriptions
itom.dataObject.axisOffsets
itom.dataObject.axisScales
itom.dataObject.axisUnits
itom.dataObject.base
itom.dataObject.conj() -> complex-conjugates all elements of this dataObject (inline).
itom.dataObject.conjugate() -> return a copy of this dataObject where every element is complex-conjugated.
itom.dataObject.continuous
itom.dataObject.copy(regionOnly=0) -> return a deep copy of this dataObject
itom.dataObject.createMask(shapes [, inverse = False]) -> return a uint8 data object of the same size where all pixels belonging to any shape are masked.
itom.dataObject.data() -> prints the content of the dataObject to the command line in a readable form.
itom.dataObject.deleteTag(key) -> Delete a tag specified by key from the tag dictionary.
itom.dataObject.dims
itom.dataObject.div(obj) -> a.div(b) return result of element wise division of a./b
itom.dataObject.dtype
itom.dataObject.existTag(key) -> return True if tag with given key exists, else False
itom.dataObject.eye(size [, dtype='uint8']) -> creates a 2D, square, eye-matrix.
itom.dataObject.fromNumpyColor(array) -> creates a rgba32 dataObject from a three-dimensional numpy array whose liast dimension has the size 3 or 4.
itom.dataObject.getTagListSize() -> returns the number of tags in the tag dictionary
itom.dataObject.imag() -> return a new data object with the imaginary part of the source
itom.dataObject.locateROI() -> returns information about the current region of interest of this data object
itom.dataObject.makeContinuous() -> return continuous representation of dataObject
itom.dataObject.metaDict
itom.dataObject.mul(obj) -> a.mul(b) returns element wise multiplication of a*b
itom.dataObject.name() -> returns the name of this object (dataObject)
itom.dataObject.ndim
itom.dataObject.normalize([minValue=0.0, maxValue=1.0, typestring='']) -> returns the normalization of this dataObject
itom.dataObject.ones(dims [, dtype='uint8'[, continuous = 0]]) -> creates new dataObject filled with ones.
itom.dataObject.physToPix(values [, axes]) -> returns the pixel coordinates for the given physical coordinates.
itom.dataObject.pixToPhys(values [, axes]) -> returns the physical coordinates for the given pixel coordinates.
itom.dataObject.rand([dims [, dtype='uint8'[, continuous = 0]]]) -> creates new dataObject filled with uniform distributed random values.
itom.dataObject.randN(dims [, dtype='uint8'[, continuous = 0]]) -> creates dataObject filled with gaussian distributed random values.
itom.dataObject.real() -> return a new data object with the real part of the source
itom.dataObject.reshape(newShape) -> return a reshaped shallow copy (if possible) of this dataObject.
itom.dataObject.setAxisDescription(axisNum, axisDescription) -> Set the description of the specified axis.
itom.dataObject.setAxisOffset(axisNum, axisOffset) -> Set the offset of the specified axis.
itom.dataObject.setAxisScale(axisNum, axisScale) -> Set the scale value of the specified axis.
itom.dataObject.setAxisUnit(axisNum, axisUnit) -> Set the unit of the specified axis.
itom.dataObject.setTag(key, tagvalue) -> Set the value of tag specified by key.
itom.dataObject.shape
itom.dataObject.size([index]) -> returns the size of this dataObject (tuple of the sizes in all dimensions or size in dimension indicated by optional axis index).
itom.dataObject.splitColor(color, [destinationType='uint8']) -> returns a seperated color channel of a rgba32 color data object
itom.dataObject.squeeze() -> return a squeezed shallow copy (if possible) of this dataObject.
itom.dataObject.tags
itom.dataObject.toGray([destinationType='uint8']) -> returns the rgba32 color data object as a gray-scale object
itom.dataObject.toNumpyColor([addAlphaChannel = 0]) -> convert a 2D dataObject of type 'rgba32' to a 3D 'uint8' numpy.array whose last dimension is 3 (no alpha channel) or 4.
itom.dataObject.tolist() -> return the data object as a (possibly nested) list
itom.dataObject.trans() -> return a plane-wise transposed dataObject
itom.dataObject.value
itom.dataObject.valueDescription
itom.dataObject.valueOffset
itom.dataObject.valueScale
itom.dataObject.valueUnit
itom.dataObject.xyRotationalMatrix
itom.dataObject.zeros(dims [, dtype='uint8'[, continuous = 0]]) -> creates new dataObject filled with zeros.
itom.figure([handle, [rows = 1, cols = 1]]) -> creates figure window.
itom.figure.close(handle|'all') -> static method to close any specific or all open figures (unless any figure-instance still keeps track of them)
itom.figure.docked
itom.figure.handle
itom.figure.hide() -> hides figure without deleting it
itom.figure.liveImage(cam, [areaIndex, className, properties]) -> shows a camera live image in the current or given area of this figure
itom.figure.matplotlibFigure([areaIndex, properties]) -> create matplotlib canvas
itom.figure.plot(data, [areaIndex, className, properties]) -> plots an existing dataObject, pointCloud or polygonMesh in the current or given area of this figure
itom.figure.show() -> shows figure if it is currently hidden
itom.figure.subplot(index) -> returns plotItem of desired subplot
itom.filter(name [, furtherParameters, ...]) -> invoke a filter (or algorithm) function from an algorithm-plugin.
itom.filterHelp([filterName, dictionary = 0, furtherInfos = 0]) -> generates an online help for the given filter(s).
itom.font(family [, pointSize = 0, weight = -1, italic = false) -> creates a font object.
itom.font.Black
itom.font.Bold
itom.font.DemiBold
itom.font.Light
itom.font.Normal
itom.font.family
itom.font.installedFontFamilies() -> return a list of all installed font families.
itom.font.isFamilyInstalled(family) -> checks if the given font family is installed on this computer.
itom.font.italic
itom.font.pointSize
itom.font.strikeOut
itom.font.underline
itom.font.weight
itom.gcEndTracking() -> compares the current object list of the garbage collector with the recently saved list.
itom.gcStartTracking() -> stores the current object list of the garbage collector.
itom.getAppPath() -> returns absolute path of application base directory.
itom.getCurrentPath() -> returns absolute path of current working directory.
itom.getDebugger() -> returns new reference to debugger instance
itom.getDefaultScaleableUnits() -> Get a list with the strings of the standard scalable units.
itom.getPalette(name) -> get the palette for color bars defined by name.
itom.getPaletteList(typefilter) -> get a list of color bars / palettes.
itom.getQtToolPath(toolname) -> get the absolute path of the Qt tool
itom.getScreenInfo() -> returns dictionary with information about all available screens.
itom.liveImage(cam, [className, properties]) -> show a camera live image in a new figure
itom.loadDataObject(filename, dataObject [, doNotAppendIDO]) -> load a dataObject from the harddrive.
itom.loadIDC(filename) -> load a pickled idc-file and return the content as dictionary
itom.loadMatlabMat(filename) -> loads Matlab mat-file by using scipy methods and returns the loaded dictionary.
itom.newScript() -> opens an empty, new script in the current script window.
itom.numeric:: [module]
itom.openScript(filename) -> open the given script in current script window.
itom.plot(data, [className, properties]) -> plots a dataObject, pointCloud or polygonMesh in a new figure
itom.plotHelp([plotName , dictionary = False]) -> generates an online help for the specified plot.
itom.plotItem(figure | uiItem[, subplotIdx]) -> instance of the plot or subplot of a figure.
itom.plotItem.PrimitiveCircle
itom.plotItem.PrimitiveEllipse
itom.plotItem.PrimitiveLine
itom.plotItem.PrimitiveMultiPointPick
itom.plotItem.PrimitivePoint
itom.plotItem.PrimitivePolygon
itom.plotItem.PrimitiveRectangle
itom.plotItem.PrimitiveSquare
itom.plotItem.drawAndPickElements
itom.plotItem.pickPoints(points [,maxNrPoints]) -> method to let the user pick points on a plot (only if plot supports this)
itom.plotLoaded(plotName) -> check if a certain plot widget is loaded.
itom.pluginHelp(pluginName [, dictionary = False]) -> generates an online help for the specified plugin.
itom.pluginLoaded(pluginName) -> check if a certain plugin could be successfully loaded.
itom.point([type, [xyz, [intensity, ][rgba, ][normal, ][curvature]]) -> creates new point used for class 'pointCloud'.
itom.point.PointInvalid
itom.point.PointXYZ
itom.point.PointXYZI
itom.point.PointXYZINormal
itom.point.PointXYZNormal
itom.point.PointXYZRGBA
itom.point.PointXYZRGBNormal
itom.point.curvature
itom.point.intensity
itom.point.name
itom.point.normal
itom.point.rgb
itom.point.rgba
itom.point.type
itom.point.xyz
itom.pointCloud([type] | pointCloud [,indices] | width, height [,point] | point) -> creates new point cloud.
itom.pointCloud.append(point) -> appends point or all points from given pointCloud at the end of the point cloud.
itom.pointCloud.clear() -> clears the whole point cloud
itom.pointCloud.copy() -> returns a deep copy of this point cloud.
itom.pointCloud.dense
itom.pointCloud.empty
itom.pointCloud.erase(indices) -> erases the points in point clouds indicated by indices (single number of slice)
itom.pointCloud.fields
itom.pointCloud.fromDisparity(disparity [,intensity] [,deleteNaN]) -> creates a point cloud from a given topography dataObject.
itom.pointCloud.fromTopography(topography [,intensity] [,deleteNaN = False]) -> creates a point cloud from a given topography dataObject.
itom.pointCloud.fromXYZ(X,Y,Z | XYZ) -> creates a point cloud from three X,Y,Z data objects or from one 3xMxN data object
itom.pointCloud.fromXYZI(X,Y,Z,I | XYZ,I) -> creates a point cloud from four X,Y,Z,I data objects or from one 3xMxN data object and one intensity data object
itom.pointCloud.fromXYZRGBA(X,Y,Z,color | XYZ,color) -> creates a point cloud from four X,Y,Z,color data objects or from one 3xMxN data object and one coloured data object
itom.pointCloud.height
itom.pointCloud.insert(index, values) -> inserts a single point or a sequence of points before position given by index
itom.pointCloud.moveXYZ(x = 0.0, y = 0.0, z = 0.0) -> move the x, y and z components of every point by the given values.
itom.pointCloud.name
itom.pointCloud.organized
itom.pointCloud.scaleXYZ(x = 1.0, y = 1.0, z = 1.0) -> scale the x, y and z components of every point by the given values .
itom.pointCloud.size
itom.pointCloud.toDataObject() -> returns a PxN data object, where P is determined by the point type in the point cloud. N is the number of points.
itom.pointCloud.type
itom.pointCloud.width
itom.polygonMesh([mesh, polygons]) -> creates a polygon mesh.
itom.polygonMesh.data
itom.polygonMesh.fromCloudAndPolygons(cloud, polygons) -> creates a polygon mesh from cloud and polygons.
itom.polygonMesh.fromOrganizedCloud(cloud [, triangulationType = 1]) -> creates a polygon mesh from an organized cloud using triangles.
itom.polygonMesh.fromTopography(topography [, triangulationType = 1]) -> creates a polygon mesh from a dataObject whose values are the z-components.
itom.polygonMesh.getCloud(pointType = point.PointInvalid) -> returns the point cloud of this polygon mesh converted to the desired type.
itom.polygonMesh.getPolygons() ->
itom.polygonMesh.name
itom.polygonMesh.nrOfPolygons
itom.processEvents
itom.proxy(??) [class]
itom.pythonStream(??) [doc: PythonStream objects]
itom.pythonStream.closed
itom.pythonStream.encoding
itom.pythonStream.fileno
itom.pythonStream.flush
itom.pythonStream.name
itom.pythonStream.readline
itom.pythonStream.type
itom.pythonStream.write
itom.region([x, y, w, h [,type=region.RECTANGLE]]) -> creates a rectangular or elliptical region.
itom.region.ELLIPSE
itom.region.RECTANGLE
itom.region.boundingRect
itom.region.contains(x,y[,w,h]) -> returns True if the given point or rectangle is fully contained in this region, otherwise returns False.
itom.region.createMask([boundingRegion]) -> creates mask data object based on this region and the optional boundingRegion.
itom.region.empty
itom.region.intersected(x,y,w,h | region) -> returns a region which is the intersection of a new region and this region.
itom.region.intersects(x,y,w,h | region) -> returns True if this region intersects with the given region, else False.
itom.region.rectCount
itom.region.rects
itom.region.subtracted(x,y,w,h | region) -> returns a region which is the new region subtracted from this region.
itom.region.translate(x,y) -> translateds this region by the given coordinates.
itom.region.translated(x,y) -> returns a region, translated by the given coordinates.
itom.region.united(x,y,w,h | region) -> returns a region which is the union of the given region with this region.
itom.region.xored(x,y,w,h | region) -> returns a region which is an xor combination of the given region with this region.
itom.removeButton(handle | toolbarName [, buttonName]) -> removes a button from a given toolbar.
itom.removeMenu(key | menuHandle) -> remove a menu element with the given key or handle.
itom.rgba(r, g, b [, alpha=255]) -> creates a new color value from red, green, blue and optional alpha
itom.rgba.alpha
itom.rgba.b
itom.rgba.g
itom.rgba.name
itom.rgba.r
itom.rgba.toGray() -> returns the gray value from the color (alpha is not considered)
itom.saveDataObject(filename, dataObject [, tagsAsBinary = False]) -> save a dataObject to harddrive in a xml-based file format.
itom.saveIDC(filename, dict [,overwriteIfExists = True]) -> saves the given dictionary as pickled idc-file.
itom.saveMatlabMat(filename, values[, matrixName = 'matrix']) -> save strings, numbers, arrays or combinations into a Matlab mat file.
itom.scaleValueAndUnit
itom.scriptEditor() -> opens new, empty script editor window (undocked)
itom.setApplicationCursor([cursorIndex = -1]) -> changes the itom cursor or restores the previously set cursor if -1
itom.setCurrentPath(newPath) -> set current working directory to given absolute newPath
itom.setPalette(name, entries) -> set the palette for color bars defined by name.
itom.shape([type, param1, param2, index, name]) -> creates a shape object of a specific type.
itom.shape.Circle
itom.shape.Ellipse
itom.shape.Invalid
itom.shape.Line
itom.shape.MoveLock
itom.shape.Point
itom.shape.Polygon
itom.shape.Rectangle
itom.shape.ResizeLock
itom.shape.RotateLock
itom.shape.Square
itom.shape.angleDeg
itom.shape.angleRad
itom.shape.area
itom.shape.basePoints
itom.shape.center
itom.shape.contour([applyTrafo = True, tol = -1.0]) -> return contour points as a 2xNumPoints float64 dataObject
itom.shape.flags
itom.shape.height
itom.shape.index
itom.shape.name
itom.shape.normalized() -> return a normalized shape (this has only an impact on rectangles, squares, ellipses and circles)
itom.shape.point1
itom.shape.point2
itom.shape.radius
itom.shape.region() -> Return a region object from this shape.
itom.shape.rotateDeg(array-like object) -> Rotate shape by given angle in degree (counterclockwise).
itom.shape.rotateRad(array-like object) -> Rotate shape by given angle in radians (counterclockwise).
itom.shape.transform
itom.shape.translate(array-like object) -> Translate shape by given (dx,dy) value.
itom.shape.type
itom.shape.valid
itom.shape.width
itom.showHelpViewer([collectionFile]) -> open the user documentation in the help viewer.
itom.timer(interval, callbackFunc [, argTuple, singleShot]) -> new callback timer
itom.timer.isActive() -> returns timer status
itom.timer.setInterval(interval) -> sets timer interval in [ms]
itom.timer.start() -> starts timer
itom.timer.stop() -> stops timer
itom.ui(filename, [type, dialogButtonBar, dialogButtons, childOfMainWindow, deleteOnClose, dockWidgetArea]) -> instance of user interface
itom.ui.BOTTOMDOCKWIDGETAREA
itom.ui.BUTTONBAR_HORIZONTAL
itom.ui.BUTTONBAR_NO
itom.ui.BUTTONBAR_VERTICAL
itom.ui.LEFTDOCKWIDGETAREA
itom.ui.MsgBoxAbort
itom.ui.MsgBoxApply
itom.ui.MsgBoxCancel
itom.ui.MsgBoxClose
itom.ui.MsgBoxDiscard
itom.ui.MsgBoxFirstButton
itom.ui.MsgBoxHelp
itom.ui.MsgBoxIgnore
itom.ui.MsgBoxLastButton
itom.ui.MsgBoxNo
itom.ui.MsgBoxNoButton
itom.ui.MsgBoxNoToAll
itom.ui.MsgBoxOk
itom.ui.MsgBoxOpen
itom.ui.MsgBoxReset
itom.ui.MsgBoxRestoreDefaults
itom.ui.MsgBoxRetry
itom.ui.MsgBoxSave
itom.ui.MsgBoxSaveAll
itom.ui.MsgBoxYes
itom.ui.MsgBoxYesToAll
itom.ui.RIGHTDOCKWIDGETAREA
itom.ui.TOPDOCKWIDGETAREA
itom.ui.TYPEDIALOG
itom.ui.TYPEDOCKWIDGET
itom.ui.TYPEWINDOW
itom.ui.availableWidgets() -> return a list of currently available widgets (that can be directly loaded in ui-files at runtime)
itom.ui.createNewPluginWidget(widgetName[, mandparams, optparams]) -> creates widget defined by any algorithm plugin and returns the instance of type 'ui'
itom.ui.createNewPluginWidget2(widgetName [, paramsArgs, paramsDict, type = -1, dialogButtonBar, dialogButtons, childOfMainWindow, deleteOnClose, dockWidgetArea) -> creates widget defined by any algorithm plugin and returns the instance of type 'ui'
itom.ui.getDouble(title, label, defaultValue [, min, max, decimals=3, parent]) -> shows a dialog to get a double value from the user
itom.ui.getExistingDirectory(caption, startDirectory [, options, parent]) -> opens a dialog to choose an existing directory
itom.ui.getInt(title, label, defaultValue [, min, max, step=1, parent]) -> shows a dialog to get an integer value from the user
itom.ui.getItem(title, label, stringList [, currentIndex=0, editable=True, parent]) -> shows a dialog to let the user select an item from a string list
itom.ui.getOpenFileName([caption, startDirectory, filters, selectedFilterIndex, options, parent]) -> opens dialog for chosing an existing file.
itom.ui.getOpenFileNames([caption, startDirectory, filters, selectedFilterIndex, options, parent]) -> opens dialog for chosing existing files.
itom.ui.getSaveFileName([caption, startDirectory, filters, selectedFilterIndex, options, parent]) -> opens dialog for chosing a file to save.
itom.ui.getText(title, label, defaultString [,parent]) -> opens a dialog in order to ask the user for a string
itom.ui.hide() -> hides initialized user interface
itom.ui.isVisible() -> returns true if dialog is still visible
itom.ui.msgCritical(title, text [, buttons, defaultButton, parent]) -> opens a critical message box
itom.ui.msgInformation(title, text [, buttons, defaultButton, parent]) -> opens an information message box
itom.ui.msgQuestion(title, text [, buttons, defaultButton, parent]) -> opens a question message box
itom.ui.msgWarning(title, text [, buttons, defaultButton, parent]) -> opens a warning message box
itom.ui.show([modal=0]) -> shows initialized UI-Dialog
itom.uiItem(...) -> base class representing any widget of a graphical user interface
itom.uiItem.call(slotOrPublicMethod [,argument1, argument2, ...]) -> calls any public slot of this widget or any accessible public method.
itom.uiItem.children([recursive = False]) -> returns dict with widget-based child items of this uiItem.
itom.uiItem.connect(signalSignature, callableMethod) -> connects the signal of the widget with the given callable python method
itom.uiItem.disconnect(signalSignature, callableMethod) -> disconnects a connection which must have been established with exactly the same parameters.
itom.uiItem.exists() -> returns true if widget still exists, else false.
itom.uiItem.getAttribute(attributeNumber) -> returns specified attribute of corresponding widget.
itom.uiItem.getProperty(propertyName | listOfPropertyNames) -> returns tuple of requested properties (single property or tuple of properties)
itom.uiItem.getPropertyInfo([propertyName]) -> returns information about the property 'propertyName' of this widget or all properties, if no name indicated.
itom.uiItem.getWindowFlags(flags) -> gets window flags of corresponding widget.
itom.uiItem.info([verbose = 0]) -> prints information about properties, public accessible slots and signals of the wrapped widget.
itom.uiItem.invokeKeyboardInterrupt(signalSignature) -> connects the given signal with a slot immediately invoking a python interrupt signal.
itom.uiItem.setAttribute(attributeNumber, value) -> sets attribute of corresponding widget.
itom.uiItem.setProperty(propertyDict) -> each property in the parameter dictionary is set to the dictionaries value.
itom.uiItem.setWindowFlags(flags) -> set window flags of corresponding widget.
itom.uncompressData(str) -> uncompresses the given string using the method qUncompress
itom.userGetInfo() -> return a dictionary with the current user management information.
itom.userIsAdmin() -> return True if USER has administrator status.
itom.userIsDeveloper() -> return True if USER has developer status.
itom.userIsUser() -> return True if USER has only user status.
itom.version([toggle-output [, include-plugins]])) -> retrieve complete information about itom version numbers
itom.widgetHelp([widgetName, dictionary = 0, furtherInfos = 0]) -> generates an online help for the given widget(s).
