<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>QObject</name>
    <message>
        <location filename="../sources/AbstractNode.cpp" line="87"/>
        <source>Live data source for plot</source>
        <translation>Live Datenquelle für Plot</translation>
    </message>
    <message>
        <location filename="../sources/AbstractDObjPCLFigure.cpp" line="40"/>
        <location filename="../sources/AbstractDObjPCLFigure.cpp" line="41"/>
        <location filename="../sources/AbstractDObjPCLFigure.cpp" line="42"/>
        <location filename="../sources/AbstractDObjPCLFigure.cpp" line="50"/>
        <location filename="../sources/AbstractDObjPCLFigure.cpp" line="51"/>
        <location filename="../sources/AbstractDObjPCLFigure.cpp" line="52"/>
        <location filename="../sources/AbstractDObjFigure.cpp" line="41"/>
        <source>Source data for plot</source>
        <translation>Quelldaten für Plot</translation>
    </message>
    <message>
        <location filename="../sources/AbstractDObjFigure.cpp" line="42"/>
        <source>Actual output data of plot</source>
        <translation>Aktuelle Ausgabedaten für Plot</translation>
    </message>
    <message>
        <location filename="../sources/AbstractNode.cpp" line="116"/>
        <source>Parameter: does not exist in updateParam</source>
        <translation>Der Parameter existiert nicht in &apos;updateParam&apos;</translation>
    </message>
    <message>
        <location filename="../sources/AbstractNode.cpp" line="125"/>
        <source>Running update on a locked input channel, i.e. updatePending flag is not set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sources/AbstractNode.cpp" line="130"/>
        <source>Channel is already updating</source>
        <translation type="unfinished">Der Channel wird breits aktualisiert</translation>
    </message>
    <message>
        <location filename="../sources/AbstractNode.cpp" line="248"/>
        <source>Not all parameters in list could not be found in channels, in updateChannels</source>
        <translation type="unfinished">Nicht alle Parameter in der Liste konnten in Channels gefunden werden (&apos;updateChannels&apos;)</translation>
    </message>
    <message>
        <location filename="../sources/AbstractNode.cpp" line="287"/>
        <source>channel is not a sender in setUpdatePending</source>
        <translation type="unfinished">Channel ist kein Sender in &apos;setUpdatePending&apos;</translation>
    </message>
    <message>
        <location filename="../sources/AbstractNode.cpp" line="292"/>
        <source>unknown channel in setUpdatePending</source>
        <translation type="unfinished">Unbekannter Channel in &apos;setUpdatePending&apos;</translation>
    </message>
    <message>
        <location filename="../sources/AbstractFigure.cpp" line="199"/>
        <location filename="../sources/AbstractFigure.cpp" line="245"/>
        <source>duplicate Channel, in addChannel</source>
        <translation type="unfinished">Doppelter Channel in &apos;addChannel&apos;</translation>
    </message>
    <message>
        <location filename="../sources/AbstractFigure.cpp" line="207"/>
        <location filename="../sources/AbstractFigure.cpp" line="214"/>
        <source>parameters incompatible, while adding channel</source>
        <translation type="unfinished">Inkompatible Parameter beim hinzufügen des Channels</translation>
    </message>
    <message>
        <location filename="../sources/AbstractFigure.cpp" line="219"/>
        <source>undefined channel direction, while adding channel</source>
        <translation type="unfinished">Undefinierte Channel-Richtung beim hinzufügen des Channels</translation>
    </message>
    <message>
        <location filename="../sources/AbstractFigure.cpp" line="234"/>
        <source>invalid child pointer, in addChannel</source>
        <translation type="unfinished">Ungültiger Pointer in &apos;addChannel&apos;</translation>
    </message>
    <message>
        <location filename="../sources/AbstractFigure.cpp" line="261"/>
        <location filename="../sources/AbstractFigure.cpp" line="286"/>
        <source>channel does not exist</source>
        <translation type="unfinished">Channel existiert nicht</translation>
    </message>
    <message>
        <source>No unit specified</source>
        <translation type="obsolete">Keine Einheiten festgelegt</translation>
    </message>
    <message>
        <source>Can&apos;t open xml file</source>
        <translation type="obsolete">XML-Datei kann nicht geöffnet werden</translation>
    </message>
    <message>
        <source>Error during setPosRel: Vectors differ in size</source>
        <translation type="obsolete">Fehler bei setPosRel: Vektoren unterscheiden sich in der Größe</translation>
    </message>
    <message>
        <source>Error during setPosAbs: Vectors differ in size</source>
        <translation type="obsolete">Fehler bei setPosAbs: Vektoren unterscheiden sich in der Größe</translation>
    </message>
    <message>
        <source>array index out of bounds.</source>
        <translation type="obsolete">Array-Index liegt außerhalb des Bereichs.</translation>
    </message>
    <message>
        <source>invalid parameter name</source>
        <translation type="obsolete">Ungültiger Parametername</translation>
    </message>
</context>
<context>
    <name>ito::AbstractDObjFigure</name>
    <message>
        <location filename="../sources/AbstractDObjFigure.cpp" line="127"/>
        <source>Function &apos;spawnLinePlot&apos; not supported from this plot widget</source>
        <translation>Die Funktion  &apos;spawnLinePlot&apos; wird von diesem Plot-Widget nicht unterstützt</translation>
    </message>
    <message>
        <location filename="../sources/AbstractDObjFigure.cpp" line="265"/>
        <source>Figure does not contain an input slot for live sources</source>
        <translation>Der Plot besitzt keinen Slot für Live-Quellen</translation>
    </message>
</context>
<context>
    <name>ito::AbstractDObjPclFigure</name>
    <message>
        <location filename="../sources/AbstractDObjPCLFigure.cpp" line="193"/>
        <source>Function &apos;spawnLinePlot&apos; not supported from this plot widget</source>
        <translation>Die Funktion  &apos;spawnLinePlot&apos; wird von diesem Plot-Widget nicht unterstützt</translation>
    </message>
</context>
<context>
    <name>ito::AbstractFigure</name>
    <message>
        <location filename="../sources/AbstractFigure.cpp" line="157"/>
        <source>Properties</source>
        <translation>Eigenschaften</translation>
    </message>
</context>
</TS>
